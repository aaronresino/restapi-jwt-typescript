import { model, Schema, Document } from 'mongoose';
import bcrypt from "bcrypt";

export interface IUser extends Document {
    email: string;
    password: string;
    comparePasswords: (password: string) => Promise<boolean>;
}

const userSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    }
});

// Cada vez que guarde un usuario, antes de guardarlo codifica la contraseña con bcrypt.
userSchema.pre<IUser>("save", async function(next) {
    const userSchema = this;
    if (!userSchema.isModified("password")) {
        return next();
    }

    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(userSchema.password, salt);
    userSchema.password = hash;
    next();
});

// Método para comparar si coinciden las contraseñas.
userSchema.methods.comparePasswords = async function(password: string): Promise<boolean> {
    return await bcrypt.compare(password, this.password);
};

export default model<IUser>("User", userSchema);